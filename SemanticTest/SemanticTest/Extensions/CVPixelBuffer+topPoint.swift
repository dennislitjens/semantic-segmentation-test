//
//  CVPixelBuffer+topPoint.swift
//  SemanticTest
//
//  Created by Dennis Litjens on 11/12/2019.
//

import CoreVideo

extension CVPixelBuffer {
    func searchTopPoint() -> CGPoint? {
        let width = CVPixelBufferGetWidth(self)
        let height = CVPixelBufferGetHeight(self)

        let bytesPerRow = CVPixelBufferGetBytesPerRow(self)
        CVPixelBufferLockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))

        defer {
            CVPixelBufferUnlockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))
        }

        var returnPoint: CGPoint?
        var whitePixelsCount = 0

        if let baseAdress = CVPixelBufferGetBaseAddress(self) {
            let buffer = baseAdress.assumingMemoryBound(to: UInt8.self)

            for y in (0..<height).reversed() {
                for x in (0..<width).reversed() {
                    let pixel = buffer[y * bytesPerRow + x * 4]
                    let abovePixel = buffer[min(y + 1, height) * bytesPerRow + x * 4]
                    let belowPixel = buffer[max(y - 1, 0) * bytesPerRow + x * 4]
                    let rightPixel = buffer[y * bytesPerRow + min(x + 1, width) * 4]
                    let leftPixel = buffer[y * bytesPerRow + max(x - 1, 0) * 4]

                    if pixel > 0 && abovePixel > 0 && belowPixel > 0 && rightPixel > 0 && leftPixel > 0 {
                        let newPoint = CGPoint(x: x, y: y)
                        returnPoint = CGPoint(x: newPoint.x / CGFloat(width), y: CGFloat(newPoint.y) / CGFloat(height))
                        whitePixelsCount += 1
                    }
                }
            }
        }

        if whitePixelsCount < 10 {
            returnPoint = nil
        }

        return returnPoint
    }
}
