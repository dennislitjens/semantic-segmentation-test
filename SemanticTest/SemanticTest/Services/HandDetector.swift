//
//  HandDetector.swift
//  SemanticTest
//
//  Created by Dennis Litjens on 10/12/2019.
//

import CoreML
import Vision

class HandDetector {
    let visionQueue = DispatchQueue(label: "be.appfoundry.visionqueue")
    private lazy var predictionRequest: VNCoreMLRequest = {
        do {
            let model = try VNCoreMLModel(for: HandModel().model)
            let request = VNCoreMLRequest(model: model)
            request.imageCropAndScaleOption = VNImageCropAndScaleOption.scaleFill
            return request
        } catch {
            fatalError("Can not create request from model.")
        }
    }()

    public func performDetection(inputBuffer: CVPixelBuffer, completion: @escaping (_ outputBuffer: CVPixelBuffer?, _ error: Error?) -> Void) {
        let requestHandler = VNImageRequestHandler(cvPixelBuffer: inputBuffer, orientation: .right)

        visionQueue.async {
            do {
                try requestHandler.perform([self.predictionRequest])

                guard let observation = self.predictionRequest.results?.first as? VNPixelBufferObservation else {
                    fatalError("Unexpected result type from VNCoreMLRequest")
                }
                completion(observation.pixelBuffer, nil)
            } catch {
                completion(nil, error)
            }
        }
    }

}
