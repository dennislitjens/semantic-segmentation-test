//
//  FlatHandGravityNode.swift
//  SemanticTest
//
//  Created by Dennis Litjens on 03/01/2020.
//

import SceneKit

public class FlatHandGravityNode: SCNNode {

    public init(radius: CGFloat) {
        super.init()
        commonInit(radius: radius)
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit(radius: 0.01)
    }

    private func commonInit(radius: CGFloat) {
        let sphere = SCNSphere(radius: 0.01)

        let material = SCNMaterial()
        material.diffuse.contents = UIColor.red
        geometry?.firstMaterial = material
//        geometry = sphere
        sphere.firstMaterial = material

        let sphereShape = SCNPhysicsShape(geometry: sphere, options: nil)
        physicsBody = SCNPhysicsBody(type: .kinematic, shape: sphereShape)
        physicsField = .radialGravity()
    }
}
