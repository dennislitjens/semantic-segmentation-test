//
//  FingerTouchNode.swift
//  SemanticTest
//
//  Created by Dennis Litjens on 11/12/2019.
//

import SceneKit

public class TouchNode: SCNNode {

    public override init() {
        super.init()
        commonInit()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        let sphere = SCNSphere(radius: 0.01)

        let material = SCNMaterial()
        material.diffuse.contents = UIColor.red
        geometry?.firstMaterial = material
//        geometry = sphere
        sphere.firstMaterial = material

        let sphereShape = SCNPhysicsShape(geometry: sphere, options: nil)
        physicsBody = SCNPhysicsBody(type: .kinematic, shape: sphereShape)
    }
}
