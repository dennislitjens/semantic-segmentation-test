//
//  BallNode.swift
//  SemanticTest
//
//  Created by Dennis Litjens on 10/12/2019.
//

import SceneKit

class BallNode: SCNNode {
    public convenience init(radius: CGFloat) {
        self.init()
        let sphere = SCNSphere(radius: radius)
        let reflectiveMaterial = SCNMaterial()
        reflectiveMaterial.lightingModel = .physicallyBased
        reflectiveMaterial.metalness.contents = 1.0
        reflectiveMaterial.roughness.contents = 0.0
        sphere.firstMaterial = reflectiveMaterial
        geometry = sphere
        physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
    }
}
