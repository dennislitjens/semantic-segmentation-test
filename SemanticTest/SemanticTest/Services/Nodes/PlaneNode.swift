//
//  PlaneNode.swift
//  SemanticTest
//
//  Created by Dennis Litjens on 10/12/2019.
//

import ARKit
import SceneKit

class PlaneNode: SCNNode {
    public func update(from planeAnchor: ARPlaneAnchor) {
        guard let device = MTLCreateSystemDefaultDevice(), let planeGeometry = ARSCNPlaneGeometry(device: device) else { fatalError() }

        let material = SCNMaterial()
        material.lightingModel = .constant
        material.writesToDepthBuffer = true
        material.colorBufferWriteMask = []
        planeGeometry.firstMaterial = material
        planeGeometry.update(from: planeAnchor.geometry)

        geometry = planeGeometry

        let shape = SCNPhysicsShape(geometry: planeGeometry, options: [SCNPhysicsShape.Option.type: SCNPhysicsShape.ShapeType.boundingBox, SCNPhysicsShape.Option.collisionMargin: 0.0])
        physicsBody = SCNPhysicsBody(type: .static, shape: shape)

        scale = SCNVector3(0.9, 1.0, 0.9)
    }
}
