//
//  SpotlightNode.swift
//  SemanticTest
//
//  Created by Dennis Litjens on 10/12/2019.
//

import SceneKit

class SpotlightNode: SCNNode {

    public override init() {
        super.init()
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        let spotlight = SCNLight()
        spotlight.type = .directional
        spotlight.shadowMode = .deferred
        spotlight.castsShadow = true
        spotlight.shadowRadius = 100.0
        spotlight.shadowColor = UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2)
        light = spotlight
        eulerAngles = SCNVector3(-Float.pi / 2, 0, 0)
    }
}
