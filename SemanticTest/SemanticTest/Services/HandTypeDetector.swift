//
//  HandTypeDetector.swift
//  SemanticTest
//
//  Created by Dennis Litjens on 02/01/2020.
//

import CoreML
import Vision
import UIKit

class HandTypeDetector {
    let visionQueue = DispatchQueue(label: "be.appfoundry.handtype-queue")
    private lazy var predictionRequest: VNCoreMLRequest = {
        do {
            let model = try VNCoreMLModel(for: example_5s0_hand_model().model)
            let request = VNCoreMLRequest(model: model) { [weak self] request, error in
                self?.processClassification(for: request, error: error)
            }
            request.imageCropAndScaleOption = VNImageCropAndScaleOption.scaleFill
            return request
        } catch {
            fatalError("Can not create request from model.")
        }
    }()

    public func performDetection(ciImage: CIImage) {
        let handler = VNImageRequestHandler(ciImage: ciImage)
        do {
            try handler.perform([self.predictionRequest])
        } catch {
            print("Failed to perform classification: \(error.localizedDescription)")
        }
    }

    public func processClassification(for request: VNRequest, error: Error?) {
        visionQueue.async {
            if let results = request.results {
                print(results.first)
            }
        }
    }
}
