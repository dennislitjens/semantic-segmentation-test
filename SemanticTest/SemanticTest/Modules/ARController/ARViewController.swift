//
//  ARViewController.swift
//  SemanticTest
//
//  Created by Dennis Litjens on 10/12/2019.
//

import UIKit
import ARKit
import CoreML
import Vision

class ARViewController: UIViewController, ARSessionDelegate, ARSCNViewDelegate {

    var previewImageView = UIImageView()
    let arSceneView = ARSCNView()
    var normalizedFingerTip: CGPoint?
    let touchNode = TouchNode()
    let ball = BallNode(radius: 0.05)

    var pixelBuffer: CVPixelBuffer?
    let handDetector = HandDetector()
    let handTypeDetector = HandTypeDetector()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func loadView() {
        super.loadView()

        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        self.view = self.arSceneView
        arSceneView.session.delegate = self
        arSceneView.delegate = self
        arSceneView.session.run(configuration)
        arSceneView.autoenablesDefaultLighting = true
        arSceneView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTap(recognizer:))))

        let spotlightNode = SpotlightNode()
        spotlightNode.position = SCNVector3(10, 10, 0)
        arSceneView.scene.rootNode.addChildNode(spotlightNode)
        arSceneView.scene.rootNode.addChildNode(touchNode)
    }

    private func startDetection() {
        guard let buffer = pixelBuffer else { return }
        defer {
            self.pixelBuffer = nil
            handTypeDetector.performDetection(ciImage: CIImage(cvPixelBuffer: buffer))
        }
        handDetector.performDetection(inputBuffer: buffer) { outputBuffer, _ in
            var previewImage: UIImage?
            var normalizedFingerTip: CGPoint?

            defer {
                DispatchQueue.main.async {
                    self.previewImageView.image = previewImage
                    self.pixelBuffer = nil
                    self.touchNode.isHidden = true

                    guard let tipPoint = normalizedFingerTip else { return }

                    let imageFingerPoint = VNImagePointForNormalizedPoint(tipPoint, Int(self.view.bounds.width), Int(self.view.bounds.height))
                    let hitTestResults = self.arSceneView.hitTest(imageFingerPoint, types: .existingPlaneUsingExtent)
                    guard let hitTestResult = hitTestResults.first else { return }

                    self.touchNode.simdTransform = hitTestResult.worldTransform
                    self.touchNode.position.y += 0.01
                    self.touchNode.isHidden = false
                }
            }

            guard let outBuffer = outputBuffer else { return }
            previewImage = UIImage(ciImage: CIImage(cvPixelBuffer: outBuffer))
            normalizedFingerTip = outBuffer.searchTopPoint()
        }
    }

    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        guard pixelBuffer == nil, case .normal = frame.camera.trackingState else { return }
        pixelBuffer = frame.capturedImage
        startDetection()
    }

    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        guard let _ = anchor as? ARPlaneAnchor else { return nil }
        return PlaneNode()
    }

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor, let planeNode = node as? PlaneNode else { return }
        planeNode.update(from: planeAnchor)
    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor, let planeNode = node as? PlaneNode else { return }
        planeNode.update(from: planeAnchor)
    }

    @objc func viewDidTap(recognizer: UITapGestureRecognizer) {
        ball.removeFromParentNode()
        ball.physicsBody?.clearAllForces()
        let tapLocation = recognizer.location(in: arSceneView)
        let hitTestResults = arSceneView.hitTest(tapLocation, types: .existingPlaneUsingExtent)
        guard let hitTestResult = hitTestResults.first else { return }

        ball.simdTransform = hitTestResult.worldTransform
        ball.position.y += 0.20
        arSceneView.scene.rootNode.addChildNode(ball)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.addSubview(previewImageView)
        previewImageView.translatesAutoresizingMaskIntoConstraints = false
        previewImageView.frame = CGRect(x: 0, y: 0, width: 180, height: 180)
    }
}
